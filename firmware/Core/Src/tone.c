
#include "tone.h"
#include "tim.h"

volatile struct tone *tone_queue;

static volatile bool tone_repeat = false;
static volatile uint16_t tone_length = 0;
static volatile uint16_t tone_index = 0;

void tone_set_buzzer(volatile struct tone *tone)
{
    if (tone->note == 0)
    {
        HAL_TIM_PWM_Stop_IT(&BUZZER_PWM_TIM, BUZZER_PWM_CHANNEL_MASK);
    }
    else
    {
        SET_FREQUENCY(BUZZER_PWM_TIM, tone->note * 2);
        HAL_TIM_PWM_Start_IT(&BUZZER_PWM_TIM, BUZZER_PWM_CHANNEL_MASK);
    }
}

void tone_run(uint16_t length, bool repeat)
{
    tone_stop();
    tone_length = length;
    tone_index = 0;
    tone_repeat = repeat;

    tone_set_buzzer(&tone_queue[tone_index]);

    uint32_t cnt = __HAL_TIM_GET_COUNTER(&TONE_TIM);
    __HAL_TIM_SET_COMPARE(&TONE_TIM, TONE_CHANNEL_MASK, cnt + tone_queue[tone_index].duration * 10U);
    HAL_TIM_OC_Start_IT(&TONE_TIM, TONE_CHANNEL_MASK);
}

bool tone_is_running()
{
    return HAL_TIM_OC_GetState(&TONE_TIM) == HAL_TIM_STATE_BUSY;
}

void tone_stop()
{
    HAL_TIM_PWM_Stop_IT(&BUZZER_PWM_TIM, BUZZER_PWM_CHANNEL_MASK);
    HAL_TIM_OC_Stop_IT(&TONE_TIM, TONE_CHANNEL_MASK);
}

void tone_tick()
{
    if (tone_index < tone_length)
    {
        if (tone_repeat)
        {
            tone_index = tone_index % tone_length;
        }

        tone_set_buzzer(&tone_queue[tone_index]);

        volatile uint32_t pulse = __HAL_TIM_GET_COUNTER(&TONE_TIM);
        __HAL_TIM_SET_COMPARE(&TONE_TIM, TONE_CHANNEL_MASK, (pulse + tone_queue[tone_index].duration * 10U));

        tone_index++;
    }
    else
    {
        tone_stop();
    }
}
