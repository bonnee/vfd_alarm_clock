#include "alarms.h"
#include "datetime.h"
#include "dsplay/dsplay.h"
#include <string.h>

#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

#define BACKUP_START_INDEX 3
#define BACKUP_REGISTER RTC_BKP_DR19
#define BACKUP_CODE 0xAA55U

alarm_t alarms[ALARMS_MAX_AMOUNT] = {{.days = 0x00, .hour = 0, .minute = 0}};

bool alarms_is_on()
{
    return HAL_GPIO_ReadPin(SW_ALARM_STATE_GPIO_Port, SW_ALARM_STATE_Pin);
}

void alarms_restore()
{
    if (BACKUP_CODE == HAL_RTCEx_BKUPRead(&hrtc, BACKUP_REGISTER))
    {
        uint32_t buf = 0;
        for (uint8_t i = 0; i < ALARMS_MAX_AMOUNT; i++)
        {
            buf = HAL_RTCEx_BKUPRead(&hrtc, i + BACKUP_START_INDEX);
            memcpy(&alarms[i], &buf, sizeof(alarm_t));
        }
    }
}

void alarms_backup()
{
    uint32_t buf = 0;
    for (uint8_t i = 0; i < ALARMS_MAX_AMOUNT; i++)
    {
        memcpy(&buf, &alarms[i], sizeof(alarm_t));
        HAL_RTCEx_BKUPWrite(&hrtc, i + BACKUP_START_INDEX, buf);
    }

    HAL_RTCEx_BKUPWrite(&hrtc, BACKUP_REGISTER, BACKUP_CODE);
}

bool alarms_enabled(alarm_t *alarm)
{
    return alarm->days & (1 >> 7);
}

uint8_t alarms_active_count()
{
    uint8_t i;
    for (i = 0; i < ALARMS_MAX_AMOUNT; i++)
    {
        if (!alarms_enabled(&alarms[i]))
        {
            return i;
        }
    }
    return i;
}

// void alarms_set(uint8_t days, uint8_t hour, uint8_t minute)
//{
//     uint8_t count = alarms_active_count();
//     if (count < ALARMS_MAX_AMOUNT)
//     {
//         alarms[count - 1].days = days;
//         alarms[count - 1].hour = days;
//         alarms[count - 1].minute = days;
//     }
// }

void alarms_check(RTC_DateTypeDef *date, RTC_TimeTypeDef *time)
{
    if (!alarms_is_on())
        return;

    for (uint8_t a = 0; a < ALARMS_MAX_AMOUNT; a++)
    {
        alarm_t alarm = alarms[a];

        if ((alarm.days & (1 << datetime_get_weekday(YEAR(date->Year), date->Month, date->Date))) && time->Hours == alarm.hour && time->Minutes == alarm.minute)
        {
            fsm_trigger_event(dsplay, DSPLAY_EV_ALARM);
        }
    }
}

alarm_t *alarms_get_next()
{
    alarm_t *next;
    //
    // RTC_TimeTypeDef now_t;
    // RTC_DateTypeDef now_d;
    // datetime_time(&now_t);
    // datetime_date(&now_d);
    //
    //// Convert now to time_t
    // struct tm now_tm = {.tm_year = now_d.Year - 1900, .tm_mon = now_d.Month, .tm_mday = now_d.Date, .tm_hour = now_t.Hours, .tm_min = now_t.Minutes, .tm_sec = now_t.Seconds};
    // time_t now = mktime(&now_tm);
    // now_tm = *gmtime(&now);
    //
    // for (uint8_t a = 0; a < alarms_count(); a++)
    //{
    //     alarm_t alarm = alarms[a];
    //
    //     struct tm alarm_tm;
    //     memcpy(&alarm_tm, &now_tm, sizeof(struct tm));
    //
    //     for (uint8_t w = 0; w < 7; w++)
    //     {
    //         // Increasing week day starting from tm_wday and rolling back to 0
    //         uint8_t day = (now_tm.tm_wday + w) % 7;
    //
    //         if (alarm.days & (1 >> day))
    //         {
    //             difftime(, now);
    //         }
    //     }
    //
    //     alarm_tm.tm_hour += alarm.hour;
    //     alarm_tm.tm_min += alarm.minute;
    //
    //     difftime(&alarm_tm, mktime(&now_tm));
    //     //alarm_t *al = &alarms[a];
    //     //uint8_t next_day = 0;
    //     //for (uint8_t weekd = 0; weekd < 7; weekd++)
    //     //    if (al->days & (1 >> weekd))
    //     //        next_day ==
    //
    //     //            next = &al;
    // }

    return next;
}
