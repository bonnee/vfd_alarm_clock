
#include "bat.h"
#include "adc.h"

static float voltage = 0;

const uint16_t voltage_soc[8][2] = {{3000, 100}, {2900, 80}, {2800, 60}, {2700, 40}, {2600, 30}, {2500, 20}, {2400, 10}, {2000, 0}};

void bat_measure()
{
    // If the interval VBAT channel is enabled an internal bridge between VBAT
    // and CH18 is established. This bridge draws a lot of current (a couple of
    // uA) from the battery. For this reason it's better to enable this bridge
    // only during voltage measurement.
    // ADC_COMMON_REGISTER(&BAT_ADC)->CCR |= ADC_CCR_VBATE;

    // The internal VBAT ADC channel reads shitty values, so CH0 is used
    // instead. No voltage dividers are used since the voltage range of CR2032
    // batteries is within spec (2-3V).
    HAL_ADC_Start(&BAT_ADC);
    HAL_ADC_PollForConversion(&BAT_ADC, HAL_MAX_DELAY);
    HAL_ADC_Stop(&BAT_ADC);

    // Disable internal VBAT bridge
    // ADC_COMMON_REGISTER(&BAT_ADC)->CCR &= ~ADC_CCR_VBATE;

    voltage = ((HAL_ADC_GetValue(&BAT_ADC) * 3.3) / 4096);
}

uint8_t bat_soc()
{
    for (uint8_t i = 0; i < sizeof(voltage_soc) / sizeof(voltage_soc[0]); i++)
    {
        float v_ref = voltage_soc[i][0] / 1000.0f;
        if (voltage > v_ref)
        {
            // If voltage > max_voltage
            if (i == 0)
            {
                return 100;
            }
            else
            {
                // Linear interpolation on voltage_soc
                return (uint8_t)(voltage_soc[i][1] +
                                 (voltage - v_ref) *
                                     (voltage_soc[i - 1][1] - voltage_soc[i][1]) /
                                     (voltage_soc[i - 1][0] / 1000.0f - v_ref));
            }
        }
    }
    return 0;
}

uint8_t bat_status()
{
    if (voltage < 2.40f)
    {
        return BAT_LOW;
    }
    return BAT_OK;
}

float bat_volt()
{
    return voltage;
}
