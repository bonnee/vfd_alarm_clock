
#include "datetime.h"
#include "main.h"

#define BACKUP_RESET 0x0000
#define BACKUP_CONFIG 0x5555

#define DATE_SRAM_REG 0x01
#define TIME_SRAM_REG 0x02

const char *weeks_names[7] = {"Mo", "Tu", "We", "Th", "Fr", "Sa", "Su"};
const char *months_names[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

struct date
{
    uint8_t day;
    uint8_t month;
    uint16_t year;
} __attribute__((__packed__));

union date_adaptor
{
    uint32_t date;
    struct date date_struct;
};

struct time
{
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;
    uint8_t subseconds;
} __attribute__((__packed__));

union time_adaptor
{
    uint32_t time;
    struct time time_struct;
};

bool datetime_load_backup()
{
    RTC_DateTypeDef date = {0};
    RTC_TimeTypeDef time = {0};

    if (HAL_RTCEx_BKUPRead(&hrtc, 0) == BACKUP_RESET)
    {
        // No backup stored
        return false;
    }

    union time_adaptor ta = {.time = HAL_RTCEx_BKUPRead(&hrtc, TIME_SRAM_REG)};
    time.Hours = ta.time_struct.hours;
    time.Minutes = ta.time_struct.minutes;
    time.Seconds = ta.time_struct.seconds;
    time.SubSeconds = ta.time_struct.subseconds;

    HAL_RTC_SetTime(&hrtc, &time, FORMAT_BIN);

    union date_adaptor da = {.date = HAL_RTCEx_BKUPRead(&hrtc, DATE_SRAM_REG)};
    date.Date = da.date_struct.day;
    date.Month = da.date_struct.month;
    date.Year = da.date_struct.year;

    HAL_RTC_SetDate(&hrtc, &date, FORMAT_BIN);

    return true;
}

void datetime_backup_date(RTC_DateTypeDef *date)
{
    assert_param(sizeof(struct date) <= 4);

    union date_adaptor da = {.date_struct.day = date->Date, .date_struct.month = date->Month, .date_struct.year = date->Year};
    uint32_t d = da.date;

    HAL_RTCEx_BKUPWrite(&hrtc, DATE_SRAM_REG, d);
    HAL_RTCEx_BKUPWrite(&hrtc, 0, BACKUP_CONFIG);
}

void datetime_backup_time(RTC_TimeTypeDef *time)
{
    assert_param(sizeof(struct time) <= 4);

    union time_adaptor ta = {.time_struct.hours = time->Hours, .time_struct.minutes = time->Minutes, .time_struct.seconds = time->Seconds, .time_struct.subseconds = time->SubSeconds};
    uint32_t t = ta.time;

    HAL_RTCEx_BKUPWrite(&hrtc, TIME_SRAM_REG, t);
    HAL_RTCEx_BKUPWrite(&hrtc, 0, BACKUP_CONFIG);
}

void datetime_date(RTC_DateTypeDef *date)
{
    HAL_RTC_GetDate(&hrtc, date, RTC_FORMAT_BIN);
    datetime_backup_date(date);
}

void datetime_time(RTC_TimeTypeDef *time)
{
    HAL_RTC_GetTime(&hrtc, time, RTC_FORMAT_BIN);
    RTC_DateTypeDef date;
    HAL_RTC_GetDate(&hrtc, &date, RTC_FORMAT_BIN);
    datetime_backup_time(time);
    datetime_backup_date(&date);
}

void datetime_set_date(RTC_DateTypeDef *date)
{
    HAL_RTC_SetDate(&hrtc, date, FORMAT_BIN);
    datetime_backup_date(date);
}

void datetime_set_time(RTC_TimeTypeDef *time)
{
    HAL_RTC_SetTime(&hrtc, time, FORMAT_BIN);
    datetime_backup_time(time);
}

uint8_t datetime_get_weekday(uint16_t year, uint8_t month, uint8_t day)
{
    uint16_t Temp1, Temp2, Temp3, Temp4, CurrentWeekDay;

    if (month < 3)
    {
        month = month + 12;
        year = year - 1;
    }

    Temp1 = (6 * (month + 1)) / 10;
    Temp2 = year / 4;
    Temp3 = year / 100;
    Temp4 = year / 400;
    CurrentWeekDay = day + (2 * month) + Temp1 + year + Temp2 - Temp3 + Temp4;

    return CurrentWeekDay % 7;
}

bool datetime_is_leap(uint16_t year)
{
    if ((year % 400) == 0)
    {
        return true;
    }
    else if ((year % 100) == 0)
    {
        return false;
    }
    else if ((year % 4) == 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}

/**
 * @brief Compares two RTC_TimeTypeDef instances
 * 
 * @param a First time instance
 * @param b Second time instance
 * @return 1 if a > b
 *         -1 if b < a
 *         0 otherwise
 */
int8_t datetime_compare_time(RTC_TimeTypeDef *a, RTC_TimeTypeDef *b)
{
    // TODO: support daylight saving

    if (a->Hours > b->Hours)
        return 1;
    else if (a->Hours < b->Hours)
        return -1;
    else
    {
        if (a->Minutes > b->Minutes)
            return 1;
        else if (a->Minutes < b->Minutes)
            return -1;
        else
        {
            if (a->Seconds > b->Seconds)
                return 1;
            else if (a->Seconds < b->Seconds)
                return -1;
            else
            {
                if (a->SubSeconds / (a->SecondFraction + 1) > b->SubSeconds / (b->SecondFraction + 1))
                    return 1;
                else if (a->SubSeconds / (a->SecondFraction + 1) < b->SubSeconds / (b->SecondFraction + 1))
                    return -1;
            }
        }
    }

    return 0;
}

uint8_t datetime_last_day(uint8_t month, uint16_t year)
{
    if (month == 4 || month == 6 || month == 9 || month == 11)
    {
        return 30;
    }
    if (month == 2)
    {
        if (datetime_is_leap(year))
        {
            return 29;
        }
        return 28;
    }

    return 31;
}

void datetime_next(RTC_DateTypeDef *date)
{
    if (date->Month == 1 || date->Month == 3 ||
        date->Month == 5 || date->Month == 7 ||
        date->Month == 8 || date->Month == 10 || date->Month == 12)
    {
        if (date->Date < 31)
        {
            date->Date++;
        }
        /* Date structure member: date->Date = 31 */
        else
        {
            if (date->Month != 12)
            {
                date->Month++;
                date->Date = 1;
            }
            /* Date structure member: date->Date = 31 & date->Month =12 */
            else
            {
                date->Month = 1;
                date->Date = 1;
                date->Year++;
            }
        }
    }
    else if (date->Month == 4 || date->Month == 6 || date->Month == 9 || date->Month == 11)
    {
        if (date->Date < 30)
        {
            date->Date++;
        }
        /* Date structure member: date->Date = 30 */
        else
        {
            date->Month++;
            date->Date = 1;
        }
    }
    else if (date->Month == 2)
    {
        if (date->Date < 28)
        {
            date->Date++;
        }
        else if (date->Date == 28)
        {
            /* Leap Year Correction */
            if (datetime_is_leap(date->Year))
            {
                date->Date++;
            }
            else
            {
                date->Month++;
                date->Date = 1;
            }
        }
        else if (date->Date == 29)
        {
            date->Month++;
            date->Date = 1;
        }
    }
}
