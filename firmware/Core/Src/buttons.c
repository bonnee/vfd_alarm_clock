#include "buttons.h"
#include "dsplay/dsplay.h"
#include "idle.h"
#include <stdbool.h>
#include <string.h>

#define BUTTON_PRESS 0
#define BUTTON_RELEASE 1

bool state[BUTTONS_COUNT] = {BUTTON_RELEASE};
const GPIO_TypeDef *buttons_gpio[BUTTONS_COUNT] = {BTN_SET_GPIO_Port, BTN_UP_GPIO_Port, BTN_DOWN_GPIO_Port, BTN_SNOOZE_GPIO_Port};
const uint16_t buttons_pin[BUTTONS_COUNT] = {BTN_SET_Pin, BTN_UP_Pin, BTN_DOWN_Pin, BTN_SNOOZE_Pin};

void buttons_tick()
{
    bool prev_state[BUTTONS_COUNT];
    memcpy(prev_state, state, sizeof(state));

    for (uint8_t i = 0; i < BUTTONS_COUNT; i++)
    {
        state[i] = HAL_GPIO_ReadPin((GPIO_TypeDef *)buttons_gpio[i], buttons_pin[i]);
        if (state[i] != prev_state[i])
        {
            if (state[i] == BUTTON_PRESS)
            {
                // Button press event
                fsm_trigger_event(dsplay, DSPLAY_EV_BTN_POSITION + i);
                idle_start_count();
            }
            if (state[i] == BUTTON_RELEASE)
            {
                // Button release event
            }
        }
    }
}
