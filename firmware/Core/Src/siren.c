#include "siren.h"
#include "math.h"

#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

void siren_arm()
{
    HAL_TIM_PWM_Start(&SIREN_TIM, SIREN_CHANNEL_MASK);
    siren_set_ppm(SIREN_MIN_SPEED);
    HAL_Delay(2500);
}

void siren_set_ppm(uint16_t value)
{
    __HAL_TIM_SET_COMPARE(&SIREN_TIM, SIREN_CHANNEL_MASK, MIN(MAX(SIREN_MIN_SPEED, value), SIREN_MAX_SPEED));
}

void siren_set_frequency(uint16_t frequency_hz)
{
    uint16_t ppm = SIREN_NOTE_LOW_PWM + round(((frequency_hz - SIREN_NOTE_LOW) * (SIREN_NOTE_HIGH_PWM - SIREN_NOTE_LOW_PWM)) / (SIREN_NOTE_HIGH - SIREN_NOTE_LOW)); // Linear extrapolation
    siren_set_ppm(ppm);
}
