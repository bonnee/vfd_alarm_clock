
#include "vfd.h"
#include <stdlib.h>

struct vfd
{
    UART_HandleTypeDef *serial;
    GPIO_TypeDef *busy_port;
    uint16_t busy_pin;
};

vfd_t vfd_new(UART_HandleTypeDef *uart, GPIO_TypeDef *busy_port, uint16_t busy_pin)
{
    vfd_t vfd = malloc(sizeof(struct vfd));
    vfd->serial = uart;
    vfd->busy_port = busy_port;
    vfd->busy_pin = busy_pin;

    return vfd;
}

bool vfd_busy(vfd_t vfd)
{
    HAL_UART_StateTypeDef uart_state = HAL_UART_GetState(vfd->serial);
    return HAL_GPIO_ReadPin(BUSY_GPIO_Port, BUSY_Pin) == GPIO_PIN_SET || uart_state != HAL_UART_STATE_READY;
}

void vfd_command(vfd_t vfd, vfd_cmd command)
{
    while (vfd_busy(vfd))
        ;
    HAL_UART_Transmit(vfd->serial, (uint8_t *)&command, 1, 10);
}

void vfd_set_dimming(vfd_t vfd, uint8_t dimming)
{
    vfd_command(vfd, VFD_DIMMING);

    while (vfd_busy(vfd))
        ;
    HAL_UART_Transmit(vfd->serial, &dimming, 1, 10);
}

void vfd_set_position(vfd_t vfd, uint8_t cursor_pos)
{
    vfd_command(vfd, VFD_DISPLAY_POSITION);

    while (vfd_busy(vfd))
        ;
    HAL_UART_Transmit(vfd->serial, &cursor_pos, 1, 10);
}

void vfd_set_cursor_mode(vfd_t vfd, vfd_cursor_mode mode)
{
    vfd_command(vfd, VFD_CURSOR_MODE);

    while (vfd_busy(vfd))
        ;
    HAL_UART_Transmit(vfd->serial, &mode, 1, 10);
}

void vfd_print(vfd_t vfd, char *text, size_t size)
{
    while (vfd_busy(vfd))
        ;
    HAL_UART_Transmit_IT(vfd->serial, (uint8_t *)text, size);
}

void vfd_printf(vfd_t vfd, ...) {}