
#include "dsplay/dsplay.h"
#include "alarms.h"
#include "dsplay/alarm.h"
#include "dsplay/aset.h"
#include "dsplay/clock.h"
#include "dsplay/dset.h"
#include "dsplay/info.h"
#include "dsplay/ssaver.h"
#include "dsplay/tset.h"
#include "idle.h"
#include "tim.h"
#include "tone.h"
#include "tones/mario_1up.h"
#include "tones/mario_coin.h"
#include "usart.h"
#include <stdio.h>

fsm dsplay;
vfd_t vfd;

uint8_t idle_state = DSPLAY_ACTIVE;

char buffer[40] = {0};

void dsplay_init()
{
    vfd = vfd_new(&huart1, BUSY_GPIO_Port, BUSY_Pin);
    vfd_command(vfd, VFD_RESET);

    dsplay = fsm_init(DSPLAY_STATE_NUM, DSPLAY_EV_NUM, dsplay_run_callback, NULL);

    fsm_state state;

    state.entry = clock_entry;
    state.run = clock_run;
    state.exit = clock_exit;
    state.handler = clock_handler;
    fsm_set_state(dsplay, DSPLAY_CLOCK, &state);

    state.entry = alarm_entry;
    state.run = alarm_run;
    state.exit = alarm_exit;
    state.handler = alarm_handler;
    fsm_set_state(dsplay, DSPLAY_ALARM, &state);

    state.entry = tset_entry;
    state.run = tset_run;
    state.exit = tset_exit;
    state.handler = tset_handler;
    fsm_set_state(dsplay, DSPLAY_TSET, &state);

    state.entry = dset_entry;
    state.run = dset_run;
    state.exit = dset_exit;
    state.handler = dset_handler;
    fsm_set_state(dsplay, DSPLAY_DSET, &state);

    state.entry = aset_entry;
    state.run = aset_run;
    state.exit = aset_exit;
    state.handler = aset_handler;
    fsm_set_state(dsplay, DSPLAY_ASET, &state);

    state.entry = info_entry;
    state.run = info_run;
    state.exit = info_exit;
    state.handler = info_handler;
    fsm_set_state(dsplay, DSPLAY_INFO, &state);

    fsm_start(dsplay);
    idle_start_count();

    if (alarms_is_on())
    {
        tone_queue = (volatile struct tone *)&mario_coin;
        tone_run(sizeof(mario_coin) / sizeof(struct tone), false);
    }
}

void dsplay_run_callback(fsm fsm)
{
    RTC_TimeTypeDef time;
    datetime_time(&time);

    static bool done = false;
    if (time.Minutes == 0 && time.Seconds == 0)
    {
        if (!done)
        {
            done = true;
            // Beep at new hour only if outside screen saver time (not at night)
            if (alarms_is_on() && !ssaver_is_time())
            {
                fsm_trigger_event(dsplay, DSPLAY_EV_HOUR_CHIME);
            }
        }
    }
    else
    {
        done = false;
    }
}

void dsplay_general_handler(fsm fsm, uint8_t event)
{
    switch (event)
    {
    case DSPLAY_EV_BTN_UP:
        fsm_transition(fsm, dsplay_menu_next(fsm_get_state(fsm)));
        break;
    case DSPLAY_EV_BTN_DOWN:
        fsm_transition(fsm, dsplay_menu_prev(fsm_get_state(fsm)));
        break;
    case DSPLAY_EV_ALARM:
        fsm_transition(fsm, DSPLAY_ALARM);
        break;
    case DSPLAY_EV_HOUR_CHIME:
        if (!tone_is_running())
        {
            tone_queue = (volatile struct tone *)&mario_1up;
            tone_run(sizeof(mario_1up) / sizeof(struct tone), false);
        }
        break;
    case DSPLAY_EV_ACTIVE:
        idle_state = DSPLAY_ACTIVE;
        vfd_set_dimming(vfd, IDLE_DIMMING_HIGH);
        break;
    case DSPLAY_EV_IDLE:
        idle_state = DSPLAY_IDLE;
        vfd_set_dimming(vfd, IDLE_DIMMING_LOW);
        break;
    case DSPLAY_EV_BTN_SNOOZE:
    case DSPLAY_EV_GO_HOME:
        if (fsm_get_state(dsplay) != DSPLAY_CLOCK)
        {
            fsm_transition(dsplay, DSPLAY_CLOCK);
        }
        break;
    }
}

uint8_t dsplay_menu_next()
{
    uint8_t cur = fsm_get_state(dsplay) + 1;

    if (cur == DSPLAY_MENU_NUM)
    {
        cur = 0;
    }

    return cur;
}

uint8_t dsplay_menu_prev()
{
    uint8_t cur = fsm_get_state(dsplay);

    if (cur == 0)
    {
        cur = DSPLAY_MENU_NUM;
    }

    return cur - 1;
}

uint8_t dsplay_get_idle_state()
{
    return idle_state;
}
