#include "dsplay/ssaver.h"
#include "dsplay/clock.h"
#include "dsplay/dsplay.h"
#include "vfd.h"
#include <stdio.h>
#include <string.h>

RTC_TimeTypeDef ssaver_start_time = {.Hours = 1};
RTC_TimeTypeDef ssaver_end_time = {.Hours = 7, .Minutes = 30};

uint8_t pos = 0;
bool ssaver_active = false;

void ssaver_update_pos()
{
    // Select random row and column assuming clock width of 5 (20 col - 5 = [0...15])
    pos = rand() % 16 + ((rand() % 2) * 20);
}

void ssaver_draw()
{
    RTC_TimeTypeDef time;
    datetime_time(&time);
    clock_draw_time(pos, true, false);

    static bool done = false;
    if (time.Seconds == 0)
    {
        if (!done)
        {
            done = true;
            ssaver_update_pos();
            vfd_command(vfd, VFD_CLEAR);
        }
    }
    else
    {
        done = false;
    }
}

/**
 * @brief
 *
 * @return true if it's time to enable the screen saver
 * @return false if no screen saver should be displayed
 */
bool ssaver_is_time()
{
    //return true; // Screen saver is always enabled
    RTC_TimeTypeDef time;
    datetime_time(&time);

    // If start time is before midnight and end time is after
    if (datetime_compare_time(&ssaver_start_time, &ssaver_end_time) >= 0)
    {
        if ((datetime_compare_time(&time, &ssaver_start_time) >= 0 && datetime_compare_time(&time, &ssaver_end_time) >= 0) || (datetime_compare_time(&time, &ssaver_start_time) <= 0 && datetime_compare_time(&time, &ssaver_end_time) <= 0))
        {
            return true;
        }
    }
    else
    {
        if (datetime_compare_time(&time, &ssaver_start_time) >= 0 && datetime_compare_time(&time, &ssaver_end_time) <= 0)
        {
            return true;
        }
    }

    return false;
}
