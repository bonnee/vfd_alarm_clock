
#include "dsplay/tset.h"
#include "datetime.h"
#include "dsplay.h"
#include "dsplay/clock.h"
#include "main.h"
#include "vfd.h"
#include <stdio.h>

#define TSET_TEXT_POS 0x06
#define TSET_TIME_POS 0x1B

typedef enum
{
    TSET_SEL_NONE = 0,
    TSET_SEL_HOURS,
    TSET_SEL_MINUTES,
    NUM_TSET_SEL
} tset_sel;

static const uint8_t positions[NUM_TSET_SEL] = {[TSET_SEL_NONE] = 0, [TSET_SEL_HOURS] = TSET_TIME_POS + 1, [TSET_SEL_MINUTES] = TSET_TIME_POS + 4};

static tset_sel selection = TSET_SEL_NONE;

static RTC_TimeTypeDef newtime = {0};

void tset_draw_newtime()
{
    vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
    clock_draw_given_time(&newtime, TSET_TIME_POS, true, false);
    vfd_set_position(vfd, positions[selection]);
    vfd_set_cursor_mode(vfd, VFD_CURSOR_BLINKING);
}

void tset_entry(fsm fsm)
{
    vfd_command(vfd, VFD_CLEAR);

    vfd_set_position(vfd, TSET_TEXT_POS);
    vfd_print(vfd, "Time Set", 8);

    selection = TSET_SEL_NONE;
}

void tset_run(fsm fsm)
{
    if (selection == TSET_SEL_NONE)
    {
        datetime_time(&newtime);
        clock_draw_given_time(&newtime, TSET_TIME_POS, true, false);
    }
}
void tset_exit(fsm fsm)
{
    vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
}

void tset_handler(fsm fsm, uint8_t event)
{
    switch (event)
    {
    case DSPLAY_EV_BTN_DOWN:
        switch (selection)
        {
        case TSET_SEL_NONE:
            dsplay_general_handler(fsm, event);
            break;
        case TSET_SEL_MINUTES:
            if (newtime.Minutes > 0)
            {
                newtime.Minutes--;
                tset_draw_newtime();
                break;
            }
            newtime.Minutes = 59;
        // No break
        case TSET_SEL_HOURS:
            if (newtime.Hours > 0)
            {
                newtime.Hours = newtime.Hours - 1;
            }
            else
            {
                newtime.Hours = 23;
            }
            tset_draw_newtime();
            break;
        default:
            break;
        }
        break;
    case DSPLAY_EV_BTN_UP:
        switch (selection)
        {
        case TSET_SEL_NONE:
            dsplay_general_handler(fsm, event);
            break;
        case TSET_SEL_MINUTES:
            if (newtime.Minutes < 59)
            {
                newtime.Minutes++;
                tset_draw_newtime();
                break;
            }
            newtime.Minutes = 0;
        case TSET_SEL_HOURS:
            newtime.Hours = (newtime.Hours + 1) % 24;
            tset_draw_newtime();
            break;
        default:
            break;
        }
        break;
    case DSPLAY_EV_BTN_SET:
        selection = (selection + 1) % NUM_TSET_SEL;
        if (selection == TSET_SEL_NONE)
        {
            newtime.Seconds = 0;
            newtime.SubSeconds = 0;
            datetime_set_time(&newtime);
            vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
        }
        else
        {
            vfd_set_position(vfd, positions[selection]);
            vfd_set_cursor_mode(vfd, VFD_CURSOR_BLINKING);
        }
        break;
    case DSPLAY_EV_ALARM:
        // Drop alarms
        break;

    default:
        dsplay_general_handler(fsm, event);
        break;
    }
}