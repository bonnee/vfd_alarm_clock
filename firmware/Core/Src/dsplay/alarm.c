#include "dsplay/alarm.h"
#include "alarms.h"
#include "datetime.h"
#include "dsplay.h"
#include "dsplay/clock.h"
#include "idle.h"
#include "main.h"
#include "tim.h"
#include "tone.h"
#include "tones/aerodynamic.h"
#include "tones/mario.h"
#include "tones/tetris.h"
#include "vfd.h"
#include <math.h>

#define ALARM_TEXT_POS 0x04
#define ALARM_TIME_POS 0x1B
static uint32_t start = 0;

void alarm_draw()
{
    clock_draw_time(ALARM_TIME_POS, false, false);
}

void alarm_entry(fsm fsm)
{
    start = HAL_GetTick();
    vfd_command(vfd, VFD_CLEAR);

    if (alarms_is_on())
    {
        tone_queue = (volatile struct tone *)&mario;
        tone_run(sizeof(mario) / sizeof(struct tone), true);
        // tone_queue = (volatile struct tone *)&tetris;
        //  uint8_t i;
        //  for (i = 0; i < 8; i += 2)
        //{
        //      tone_queue[i].note = 440;
        //      tone_queue[i].duration = 100;
        //      tone_queue[i + 1].note = 0;
        //      tone_queue[i + 1].duration = 100;
        //  }
        //  tone_queue[i - 1].duration = 500;
    }

    uint32_t cnt = __HAL_TIM_GET_COUNTER(&ALARM_TIMEOUT_TIM);
    __HAL_TIM_SET_COMPARE(&ALARM_TIMEOUT_TIM, ALARM_TIMEOUT_CHANNEL_MASK, cnt + ALARM_TIMEOUT * 10);
    HAL_TIM_OC_Start_IT(&ALARM_TIMEOUT_TIM, ALARM_TIMEOUT_CHANNEL_MASK);

    vfd_set_position(vfd, ALARM_TEXT_POS);
    vfd_print(vfd, "!! ALARM !!", 11);
    alarm_draw();
}

void alarm_run(fsm fsm)
{
    RTC_TimeTypeDef time;
    datetime_time(&time);

    float dec = modf((float)time.SubSeconds / time.SecondFraction + 1, NULL);
    if (dec >= .5f)
    {
        alarm_draw();
        vfd_set_dimming(vfd, VFD_DIMMING_100);
    }
    else if (dec < .5f)
    {
        vfd_set_dimming(vfd, VFD_DIMMING_0);
    }
}

void alarm_exit(fsm fsm)
{
    tone_stop();
    idle_start_count();
}

void alarm_handler(fsm fsm, uint8_t event)
{
    switch (event)
    {
    case DSPLAY_EV_ALARM_TIMEOUT:
    case DSPLAY_EV_BTN_SNOOZE:
        fsm_transition(fsm, DSPLAY_CLOCK);
        break;
    }
}
