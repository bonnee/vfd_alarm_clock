#include "dsplay/clock.h"
#include "alarms.h"
#include "bat.h"
#include "datetime.h"
#include "dsplay.h"
#include "dsplay/ssaver.h"
#include "tone.h"
#include "vfd.h"
#include <stdio.h>
#include <string.h>

#define CLOCK_TIME_POS_ACTIVE 0x06
#define CLOCK_TIME_POS_IDLE 0x08
#define CLOCK_DATE_POS 0x14
#define CLOCK_BAT_WARN_POS 0x13
#define CLOCK_ALARM_POS 0x1f

#define CLOCK_PRINT_TIME()                                      \
    {                                                           \
        if (dsplay_get_idle_state() == DSPLAY_ACTIVE)           \
        {                                                       \
            clock_draw_time(CLOCK_TIME_POS_ACTIVE, true, true); \
        }                                                       \
        else                                                    \
        {                                                       \
            clock_draw_time(CLOCK_TIME_POS_IDLE, true, false);  \
        }                                                       \
    }

void clock_draw_given_time(RTC_TimeTypeDef *time, uint8_t position, bool blink, bool seconds)
{
    vfd_set_position(vfd, position);

    if (seconds)
    {
        sprintf(buffer, "%02d:%02d:%02d", time->Hours, time->Minutes, time->Seconds);
    }
    else
    {
        sprintf(buffer, "%02d:%02d", time->Hours, time->Minutes);
    }

    if (blink && (time->SecondFraction / 2) > time->SubSeconds)
    {
        buffer[2] = ' ';
        if (seconds)
        {
            buffer[5] = ' ';
        }
    }

    vfd_print(vfd, buffer, strlen(buffer));
}

void clock_draw_time(uint8_t position, bool blink, bool seconds)
{
    RTC_TimeTypeDef time;
    datetime_time(&time);
    clock_draw_given_time(&time, position, blink, seconds);
}

void clock_draw_date()
{
    RTC_DateTypeDef date;
    datetime_date(&date);

    vfd_set_position(vfd, CLOCK_DATE_POS);
    sprintf(buffer, "%s %02d %s", weeks_names[datetime_get_weekday(YEAR(date.Year), date.Month, date.Date)], date.Date, months_names[date.Month - 1]);
    vfd_print(vfd, buffer, 10);
}

void clock_draw_alarms()
{
    vfd_set_position(vfd, CLOCK_ALARM_POS);
    if (alarms_is_on())
    {
        strcpy(buffer, "Alarm On ");
    }
    else
    {
        strcpy(buffer, "Alarm Off");
        uint8_t len = 0;
        for (uint8_t alarm_t = 0; alarm_t < alarms_active_count(); alarm_t++)
        {
            for (uint8_t day = 0; day < 6; day++)
            {
                if (alarms[alarm_t].days & (1 >> day))
                {
                    if (day > 0)
                    {
                        sprintf(buffer + len, " ");
                        len = strlen(buffer);
                    }
                    sprintf(buffer + len, "%s", weeks_names[alarms[alarm_t].days]);
                    len = strlen(buffer);
                }
            }
        }
    }
    vfd_print(vfd, buffer, strlen(buffer));
}

void clock_draw_battery_warning()
{
    vfd_set_position(vfd, CLOCK_BAT_WARN_POS);
    if (bat_status() == BAT_LOW)
    {
        vfd_print(vfd, "!", 1);
    }
    else
    {
        vfd_print(vfd, " ", 1);
    }
}

void clock_redraw()
{
    // Screen saver is always active
    if (/*ssaver_is_time()*/true && dsplay_get_idle_state() == DSPLAY_IDLE)
    {
        // If idle and the time is right enable screen saver
        ssaver_active = true;
        ssaver_draw();
    }
    else
    {
        if (ssaver_active)
        {
            // If screen saver was active clear the screen
            vfd_command(vfd, VFD_CLEAR);
            ssaver_active = false;
        }
        CLOCK_PRINT_TIME();
        clock_draw_date();
        clock_draw_alarms();
        clock_draw_battery_warning();
    }
}

void clock_entry(fsm fsm)
{
    vfd_command(vfd, VFD_CLEAR);
    ssaver_update_pos();
    // clock_redraw();
}

void clock_run(fsm fsm)
{
    RTC_TimeTypeDef time;
    datetime_time(&time);

    clock_redraw();
}
void clock_exit(fsm fsm) {}

void clock_handler(fsm fsm, uint8_t event)
{
    switch (event)
    {
    case DSPLAY_EV_BTN_SET:
        fsm_transition(fsm, DSPLAY_ALARM);
        break;
    case DSPLAY_EV_IDLE:
    case DSPLAY_EV_ACTIVE:
        vfd_command(vfd, VFD_CLEAR);
        // No break. go to default
    default:
        dsplay_general_handler(fsm, event);
        break;
    }
}
