
#include "dsplay/aset.h"
#include "alarms.h"
#include "datetime.h" //For weeks_names[]
#include "dsplay.h"
#include "main.h"
#include "vfd.h"
#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define ASET_TIME_POS 0x0D
#define ASET_INDEX_POS 0x06
#define ASET_PREV_POS 0x05
#define ASET_NEXT_POS 0x08
#define ASET_DAYS_POS 0x14

typedef enum
{
    ASET_SEL_NONE = 0,
    ASET_SEL_INDEX,
    ASET_SEL_HOURS,
    ASET_SEL_MINUTES_DEC,
    ASET_SEL_MINUTES,
    ASET_SEL_MO,
    ASET_SEL_TU,
    ASET_SEL_WE,
    ASET_SEL_TH,
    ASET_SEL_FR,
    ASET_SEL_SA,
    ASET_SEL_SU,
    NUM_ASET_SEL
} aset_sel;

static const uint8_t positions[NUM_ASET_SEL] = {[ASET_SEL_NONE] = 0, [ASET_SEL_INDEX] = ASET_INDEX_POS + 1, [ASET_SEL_HOURS] = ASET_TIME_POS + 3, [ASET_SEL_MINUTES_DEC] = ASET_TIME_POS + 5, [ASET_SEL_MINUTES] = ASET_TIME_POS + 6, [ASET_SEL_MO] = ASET_DAYS_POS, [ASET_SEL_TU] = ASET_DAYS_POS + 3, [ASET_SEL_WE] = ASET_DAYS_POS + 6, [ASET_SEL_TH] = ASET_DAYS_POS + 9, [ASET_SEL_FR] = ASET_DAYS_POS + 12, [ASET_SEL_SA] = ASET_DAYS_POS + 15, [ASET_SEL_SU] = ASET_DAYS_POS + 18};

static uint8_t alarm_index = 0;
static alarm_t temp_alarm = {0, 0, 0};
static aset_sel selection = ASET_SEL_NONE;

void aset_draw_index()
{
    vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
    vfd_set_position(vfd, ASET_INDEX_POS);
    sprintf(buffer, "%02d", alarm_index + 1);
    vfd_print(vfd, buffer, 2);

    vfd_set_position(vfd, ASET_PREV_POS);
    if (alarm_index > 0)
    {
        buffer[0] = (char)0b11100001;
    }
    else
    {
        buffer[0] = ' ';
    }
    vfd_print(vfd, buffer, 1);

    vfd_set_position(vfd, ASET_NEXT_POS);
    if (alarm_index < ALARMS_MAX_AMOUNT - 1)
    {
        buffer[0] = (char)0b11100000;
    }
    else
    {
        buffer[0] = ' ';
    }
    vfd_print(vfd, buffer, 1);
}

void aset_draw_time()
{
    vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
    vfd_set_position(vfd, ASET_TIME_POS);
    sprintf(buffer, "T=%02d:%02d", temp_alarm.hour, temp_alarm.minute);
    vfd_print(vfd, buffer, 7);
}

void aset_draw_days()
{
    vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
    vfd_set_position(vfd, 0x14);
    for (uint8_t i = 0; i < 7; i++)
    {
        if (temp_alarm.days & (0x1 << i))
        {
            buffer[0] = toupper(weeks_names[i][0]);
            buffer[1] = toupper(weeks_names[i][1]);
        }
        else
        {
            buffer[0] = tolower(weeks_names[i][0]);
            buffer[1] = tolower(weeks_names[i][1]);
        }
        vfd_print(vfd, buffer, 2);

        if (i < 6)
        {
            vfd_print(vfd, " ", 1);
        }
    }
}

void aset_redraw()
{
    vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
    vfd_set_position(vfd, 0);
    vfd_print(vfd, "Alarm", 6);
    aset_draw_time();
    aset_draw_index();
    aset_draw_days();
}

void aset_entry(fsm fsm)
{
    vfd_command(vfd, VFD_CLEAR);

    // vfd_set_position(vfd, 0x05);
    // vfd_print(vfd, "Alarm Set", 10);
    // HAL_Delay(500);
    // vfd_command(vfd, VFD_CLEAR);

    selection = ASET_SEL_NONE;
    alarm_index = 0;
    memcpy(&temp_alarm, &alarms[alarm_index], sizeof(alarm_t));
    aset_redraw();
}

void aset_run(fsm fsm)
{
    if (selection == ASET_SEL_NONE)
    {
        vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
    }
    else
    {
        vfd_set_position(vfd, positions[selection]);
        vfd_set_cursor_mode(vfd, VFD_CURSOR_BLINKING);
    }
}
void aset_exit(fsm fsm)
{
    vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
}

void aset_handler(fsm fsm, uint8_t event)
{
    switch (event)
    {
    case DSPLAY_EV_BTN_DOWN:
        switch (selection)
        {
        case ASET_SEL_NONE:
            dsplay_general_handler(fsm, event);
            break;
        case ASET_SEL_INDEX:
            if (alarm_index > 0)
            {
                alarm_index--;
                memcpy(&temp_alarm, &alarms[alarm_index], sizeof(alarm_t));
                aset_redraw();
            }
            break;

        case ASET_SEL_HOURS:
            if (temp_alarm.hour == 0)
                temp_alarm.hour = 24;

            temp_alarm.hour--;
            aset_draw_time();
            break;

        case ASET_SEL_MINUTES_DEC:
        {
            int8_t min = temp_alarm.minute - 10;
            if (min < 0)
            {
                min += 60;
            }

            temp_alarm.minute = min;
            aset_draw_time();
            break;
        }

        case ASET_SEL_MINUTES:
            if (temp_alarm.minute == 0)
                temp_alarm.minute = 60;

            temp_alarm.minute--;
            aset_draw_time();
            break;

        default:
            temp_alarm.days ^= 0b1 << (selection - ASET_SEL_MO);
            aset_draw_days();
            break;
        }
        break;

    case DSPLAY_EV_BTN_UP:
        switch (selection)
        {
        case ASET_SEL_NONE:
            dsplay_general_handler(fsm, event);
            break;
        case ASET_SEL_INDEX:
            if (alarm_index < ALARMS_MAX_AMOUNT - 1)
            {
                alarm_index++;
                memcpy(&temp_alarm, &alarms[alarm_index], sizeof(alarm_t));
                aset_redraw();
            }
            break;
        case ASET_SEL_HOURS:
            temp_alarm.hour = (temp_alarm.hour + 1) % 24;
            aset_draw_time();
            break;
        case ASET_SEL_MINUTES_DEC:
        {
            temp_alarm.minute += 10;
            if (temp_alarm.minute > 59)
            {
                temp_alarm.minute -= 60;
            }

            aset_draw_time();
            break;
        }
        case ASET_SEL_MINUTES:
            temp_alarm.minute = (temp_alarm.minute + 1) % 60;
            aset_draw_time();
            break;
        default:
            temp_alarm.days ^= 0b1 << (selection - ASET_SEL_MO);
            aset_draw_days();
            break;
        }
        break;

    case DSPLAY_EV_BTN_SET:
        selection = (selection + 1) % NUM_ASET_SEL;
        memcpy(&alarms[alarm_index], &temp_alarm, sizeof(alarm_t));
        alarms_backup();
        break;

    case DSPLAY_EV_BTN_SNOOZE:
        if (selection != ASET_SEL_NONE)
        {
            selection = ASET_SEL_NONE;
            memcpy(&alarms[alarm_index], &temp_alarm, sizeof(alarm_t));
            alarms_backup();
        }
        else
        {
            dsplay_general_handler(fsm, event);
        }
        break;
    default:
        dsplay_general_handler(fsm, event);
        break;
    }
}