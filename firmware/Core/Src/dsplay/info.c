#include "dsplay/info.h"
#include "alarms.h"
#include "bat.h"
#include "datetime.h"
#include "dsplay/dsplay.h"
#include "vfd.h"
#include <stdio.h>
#include <string.h>

#define INFO_ALARMS_POS 0x00
#define INFO_BAT_POS 0x18

void info_draw_alarms()
{
    vfd_set_position(vfd, INFO_ALARMS_POS);
    sprintf(buffer, "%02i alarms", alarms_active_count());
    vfd_print(vfd, buffer, strlen(buffer));
}

void info_draw_battery_volt()
{
    vfd_set_position(vfd, INFO_BAT_POS);
    sprintf(buffer, "Bat %.2fV (%3i%%)", bat_volt(), bat_soc());
    vfd_print(vfd, buffer, strlen(buffer));
}

void info_redraw()
{
    info_draw_alarms();
    info_draw_battery_volt();
}

void info_entry(fsm fsm)
{
    vfd_command(vfd, VFD_CLEAR);
    info_redraw();
}

void info_run(fsm fsm)
{
    info_redraw();
}
void info_exit(fsm fsm) {}

void info_handler(fsm fsm, uint8_t event)
{
    switch (event)
    {
    case DSPLAY_EV_BTN_SET:
        bat_measure();
        break;
    default:
        dsplay_general_handler(fsm, event);
        break;
    }
}
