
#include "dsplay/dset.h"
#include "datetime.h"
#include "dsplay.h"
#include "main.h"
#include "vfd.h"
#include <stdio.h>
#include <string.h>

#define DSET_TEXT_POS 0x6
#define DSET_DATE_POS 0x18

#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a, b) ((a) < (b) ? (a) : (b))
#endif

typedef enum
{
    DSET_SEL_NONE = 0,
    DSET_SEL_DAY,
    DSET_SEL_MONTH,
    DSET_SEL_YEAR,
    NUM_DSET_SEL
} dset_sel;

static const uint8_t positions[NUM_DSET_SEL] = {[DSET_SEL_NONE] = 0, [DSET_SEL_DAY] = DSET_DATE_POS + 4, [DSET_SEL_MONTH] = DSET_DATE_POS + 8, [DSET_SEL_YEAR] = DSET_DATE_POS + 11};
static dset_sel selection = DSET_SEL_NONE;

static RTC_DateTypeDef newdate = {0};

void dset_draw_newdate(RTC_DateTypeDef *date)
{
    vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
    vfd_set_position(vfd, DSET_DATE_POS);
    sprintf(buffer, "%s %02d %s %02d", weeks_names[datetime_get_weekday(YEAR(date->Year), date->Month, date->Date)], date->Date, months_names[date->Month - 1], date->Year);
    vfd_print(vfd, buffer, strlen(buffer));
    vfd_set_position(vfd, positions[selection]);
    if (selection != DSET_SEL_NONE)
    {
        vfd_set_cursor_mode(vfd, VFD_CURSOR_BLINKING);
    }
}

void dset_entry(fsm fsm)
{
    vfd_command(vfd, VFD_CLEAR);

    vfd_set_position(vfd, DSET_TEXT_POS);
    vfd_print(vfd, "Date Set", 8);

    selection = DSET_SEL_NONE;
}

void dset_run(fsm fsm)
{
    if (selection == DSET_SEL_NONE)
    {
        datetime_date(&newdate);
        dset_draw_newdate(&newdate);
    }
}
void dset_exit(fsm fsm)
{
    vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
}

void dset_handler(fsm fsm, uint8_t event)
{
    switch (event)
    {
    case DSPLAY_EV_BTN_DOWN:
        switch (selection)
        {
        case DSET_SEL_NONE:
            dsplay_general_handler(fsm, event);
            break;
        case DSET_SEL_DAY:
            if (newdate.Date > 1)
            {
                newdate.Date--;
            }
            else
            {
                newdate.Date = datetime_last_day(newdate.Month, YEAR(newdate.Year));
            }
            dset_draw_newdate(&newdate);
            break;
        case DSET_SEL_MONTH:
            if (newdate.Month > 1)
            {
                newdate.Month--;
            }
            else
            {
                newdate.Month = 12;
            }
            newdate.Date = MIN(newdate.Date, datetime_last_day(newdate.Month, YEAR(newdate.Year)));
            dset_draw_newdate(&newdate);
            break;
        case DSET_SEL_YEAR:
            if (newdate.Year > 0)
            {
                newdate.Year--;
            }
            else
            {
                newdate.Year = 99;
            }
            newdate.Date = MIN(newdate.Date, datetime_last_day(newdate.Month, YEAR(newdate.Year)));
            dset_draw_newdate(&newdate);
            break;
        default:
            break;
        }
        break;
    case DSPLAY_EV_BTN_UP:
        switch (selection)
        {
        case DSET_SEL_NONE:
            dsplay_general_handler(fsm, event);
            break;
        case DSET_SEL_DAY:
            if (newdate.Date < datetime_last_day(newdate.Month, YEAR(newdate.Year)))
            {
                newdate.Date++;
            }
            else
            {
                newdate.Date = 1;
            }
            dset_draw_newdate(&newdate);
            break;
        case DSET_SEL_MONTH:
            if (newdate.Month < 12)
            {
                newdate.Month++;
            }
            else
            {
                newdate.Month = 1;
            }
            newdate.Date = MIN(newdate.Date, datetime_last_day(newdate.Month, YEAR(newdate.Year)));
            dset_draw_newdate(&newdate);
            break;
        case DSET_SEL_YEAR:
            newdate.Year = (newdate.Year + 1) % 99;
            newdate.Date = MIN(newdate.Date, datetime_last_day(newdate.Month, YEAR(newdate.Year)));
            dset_draw_newdate(&newdate);
            break;
        default:
            break;
        }
        break;
    case DSPLAY_EV_BTN_SET:
        selection = (selection + 1) % NUM_DSET_SEL;
        if (selection == DSET_SEL_NONE)
        {
            datetime_set_date(&newdate);
            vfd_set_cursor_mode(vfd, VFD_CURSOR_OFF);
        }
        else
        {
            vfd_set_position(vfd, positions[selection]);
            vfd_set_cursor_mode(vfd, VFD_CURSOR_BLINKING);
        }
        break;
    case DSPLAY_EV_ALARM:
        // Drop alarms
        break;

    default:
        dsplay_general_handler(fsm, event);
        break;
    }
}
