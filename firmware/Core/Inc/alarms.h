#pragma once
#include "rtc.h"
#include <inttypes.h>
#include <stdbool.h>

#define ALARMS_MAX_AMOUNT 10

enum
{
    ALARMS_DAY_MONDAY = 0x1 << (RTC_WEEKDAY_MONDAY - 1),
    ALARMS_DAY_TUESDAY = 0x1 << (RTC_WEEKDAY_TUESDAY - 1),
    ALARMS_DAY_WEDNESDAY = 0x1 << (RTC_WEEKDAY_WEDNESDAY - 1),
    ALARMS_DAY_THURSDAY = 0x1 << (RTC_WEEKDAY_THURSDAY - 1),
    ALARMS_DAY_FRIDAY = 0x1 << (RTC_WEEKDAY_FRIDAY - 1),
    ALARMS_DAY_SATURDAY = 0x1 << (RTC_WEEKDAY_SATURDAY - 1),
    ALARMS_DAY_SUNDAY = 0x1 << (RTC_WEEKDAY_SUNDAY - 1),
};

typedef struct
{
    uint8_t days;
    uint8_t hour;
    uint8_t minute;
} __attribute__((__packed__)) alarm_t;

extern alarm_t alarms[ALARMS_MAX_AMOUNT];

void alarms_restore();
void alarms_backup();

bool alarms_is_on();
bool alarms_enabled(alarm_t *alarm);
uint8_t alarms_active_count();
void alarms_check(RTC_DateTypeDef *date, RTC_TimeTypeDef *time);
