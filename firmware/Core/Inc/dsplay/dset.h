#pragma once
#include <fsm.h>
#include <inttypes.h>

void dset_entry(fsm fsm);
void dset_run(fsm fsm);
void dset_exit(fsm fsm);
void dset_handler(fsm fsm, uint8_t event);
