#pragma once
#include "rtc.h"
#include <fsm.h>
#include <inttypes.h>

void info_entry(fsm fsm);
void info_run(fsm fsm);
void info_exit(fsm fsm);
void info_handler(fsm fsm, uint8_t event);
