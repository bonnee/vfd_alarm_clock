
#pragma once

#include "vfd.h"
#include <fsm.h>

#define DSPLAY_REFRESH_TIME 50 // ms

#define DSPLAY_MENU_NUM 5 // How many menus are part of the main carousel

enum
{
    DSPLAY_CLOCK = 0,
    DSPLAY_ASET,
    DSPLAY_INFO,
    DSPLAY_DSET,
    DSPLAY_TSET,

    DSPLAY_ALARM,

    DSPLAY_STATE_NUM
};

enum
{
    DSPLAY_EV_ALARM = 0,
    DSPLAY_EV_ALARM_TIMEOUT,
    DSPLAY_EV_HOUR_CHIME,

    DSPLAY_EV_BTN_SET,
    DSPLAY_EV_BTN_UP,
    DSPLAY_EV_BTN_DOWN,
    DSPLAY_EV_BTN_SNOOZE,
    DSPLAY_EV_BTN_MUTE,

    DSPLAY_EV_ACTIVE,
    DSPLAY_EV_IDLE,
    DSPLAY_EV_GO_HOME,

    DSPLAY_EV_NUM
};

// Position of the button events in the event array
#define DSPLAY_EV_BTN_POSITION DSPLAY_EV_BTN_SET

enum
{
    DSPLAY_IDLE = 0,
    DSPLAY_ACTIVE
};

extern fsm dsplay;
extern vfd_t vfd;
extern char buffer[40];

void dsplay_init();

void dsplay_run_callback(fsm fsm);
uint8_t dsplay_menu_next();
uint8_t dsplay_menu_prev();
void dsplay_general_handler(fsm fsm, uint8_t event);

uint8_t dsplay_get_idle_state();
