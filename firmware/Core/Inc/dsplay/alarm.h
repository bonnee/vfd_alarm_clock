#pragma once
#include "main.h"
#include <fsm.h>
#include <inttypes.h>

#define ALARM_TIMEOUT 300000U

void alarm_entry(fsm fsm);
void alarm_run(fsm fsm);
void alarm_exit(fsm fsm);
void alarm_handler(fsm fsm, uint8_t event);
