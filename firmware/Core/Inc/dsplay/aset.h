#pragma once
#include <fsm.h>
#include <inttypes.h>

void aset_entry(fsm fsm);
void aset_run(fsm fsm);
void aset_exit(fsm fsm);
void aset_handler(fsm fsm, uint8_t event);
