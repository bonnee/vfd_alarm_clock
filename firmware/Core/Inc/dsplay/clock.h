#pragma once
#include "rtc.h"
#include <fsm.h>
#include <inttypes.h>

void clock_draw_time(uint8_t position, bool blink, bool seconds);
void clock_draw_given_time(RTC_TimeTypeDef *time, uint8_t position, bool blink, bool seconds);
void clock_entry(fsm fsm);
void clock_run(fsm fsm);
void clock_exit(fsm fsm);
void clock_handler(fsm fsm, uint8_t event);
