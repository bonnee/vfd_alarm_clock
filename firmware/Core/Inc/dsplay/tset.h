#pragma once
#include <fsm.h>
#include <inttypes.h>

void tset_entry(fsm fsm);
void tset_run(fsm fsm);
void tset_exit(fsm fsm);
void tset_handler(fsm fsm, uint8_t event);
