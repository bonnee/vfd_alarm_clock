#pragma once
#include "datetime.h"
#include <fsm.h>
#include <inttypes.h>
#include <stdbool.h>

extern RTC_TimeTypeDef ssaver_start_time;
extern RTC_TimeTypeDef ssaver_end_time;
extern bool ssaver_active;

void ssaver_update_pos();
void ssaver_draw();
bool ssaver_is_time();
