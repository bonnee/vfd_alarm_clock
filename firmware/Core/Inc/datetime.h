#pragma once
#include "rtc.h"
#include <inttypes.h>
#include <stdbool.h>

// Maybe I'm overestimating the longevity of my code
#define CENTURY 21U
#define YEAR(y) ((uint16_t)(y) + ((CENTURY - 1) * 100U))

extern const char *weeks_names[7];
extern const char *months_names[12];

bool datetime_load_backup();
void datetime_date(RTC_DateTypeDef *date);
void datetime_time(RTC_TimeTypeDef *time);
void datetime_set_date(RTC_DateTypeDef *date);
void datetime_set_time(RTC_TimeTypeDef *time);
uint8_t datetime_get_weekday(uint16_t year, uint8_t month, uint8_t day);
uint8_t datetime_last_day(uint8_t month, uint16_t year);

int8_t datetime_compare_time(RTC_TimeTypeDef *a, RTC_TimeTypeDef *b);
