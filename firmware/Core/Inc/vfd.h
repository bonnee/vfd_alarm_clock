
#pragma once

#include "main.h"
#include <inttypes.h>
#include <stdbool.h>

typedef struct vfd *vfd_t;

typedef enum
{
    VFD_DIMMING = 0x04,
    VFD_BACKSPACE = 0x08,
    VFD_TAB = 0x09,
    VFD_CLEAR = 0x0D,
    VFD_DISPLAY_POSITION = 0x10,
    VFD_CURSOR_MODE = 0x17,
    VFD_ALL_DISPLAY = 0x0F,
    VFD_RESET = 0x1F
} vfd_cmd;

typedef enum
{
    VFD_DIMMING_0 = 0,
    VFD_DIMMING_20 = 0x20,
    VFD_DIMMING_40 = 0x40,
    VFD_DIMMING_60 = 0x60,
    VFD_DIMMING_80 = 0x80,
    VFD_DIMMING_100 = 0xFF,
} vfd_dimming;

typedef enum
{
    VFD_CURSOR_OFF = 0,
    VFD_CURSOR_BLINKING = 0x88,
    VFD_CURSOR_LIGHTING = 0xFF
} vfd_cursor_mode;

vfd_t vfd_new(UART_HandleTypeDef *uart, GPIO_TypeDef *busy_port, uint16_t busy_pin);
bool vfd_busy(vfd_t vfd);

void vfd_command(vfd_t vfd, vfd_cmd command);
void vfd_set_dimming(vfd_t vfd, uint8_t dimming);
void vfd_set_position(vfd_t vfd, uint8_t cursor_pos);
void vfd_set_cursor_mode(vfd_t vfd, vfd_cursor_mode mode);
void vfd_print(vfd_t vfd, char *text, size_t size);