
#pragma once

#include "tim.h"
#include <inttypes.h>
#include <stdbool.h>

#define SIREN_NOTE_LOW 330.f // A3
#define SIREN_NOTE_LOW_PWM 1298

#define SIREN_NOTE_HIGH 440.f // A5
#define SIREN_NOTE_HIGH_PWM 1404

#define SIREN_MIN_SPEED 1000
#define SIREN_MAX_SPEED 2000

// Music notes
#define C4 262
#define C4s 277
#define D4 294
#define D4s 311
#define E4 330
#define F4s 370
#define G4 392
#define G4s 415
#define A4 440
#define A4s 466
#define B4 494

/**
 * @brief Arms the ESC, getting it ready to run
 * @note This function is blocking
 */
void siren_arm();

/**
 * @brief Sets the pulse position modulation of the siren, regulating its speed.
 *
 * @param value PPM value to set
 */
void siren_set_ppm(uint16_t value);

/**
 * @brief Sets the frequency that will be emitted by the siren
 *
 * @param frequency_hz The frequency in Hz
 */
void siren_set_frequency(uint16_t frequency_hz);
