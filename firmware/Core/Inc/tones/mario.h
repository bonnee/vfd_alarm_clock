#pragma once
#include "tone.h"

#define MARIO_LENGTH 1492

// https://www.kirrus.co.uk/2010/09/linux-beep-music/
static const volatile struct tone mario[MARIO_LENGTH] = {
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 47},
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 189},
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 190},
    {146, 20},
    {369, 20},
    {523, 20},
    {0, 47},
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 190},
    {391, 20},
    {493, 20},
    {783, 20},
    {0, 475},
    {195, 20},
    {391, 20},
    {195, 20},
    {391, 20},
    {0, 473},
    {195, 20},
    {329, 20},
    {523, 20},
    {0, 332},
    {164, 20},
    {261, 20},
    {391, 20},
    {0, 332},
    {130, 20},
    {195, 20},
    {329, 20},
    {0, 332},
    {174, 20},
    {261, 20},
    {440, 20},
    {0, 189},
    {195, 20},
    {293, 20},
    {493, 20},
    {0, 189},
    {184, 20},
    {277, 20},
    {466, 20},
    {0, 47},
    {174, 20},
    {261, 20},
    {440, 20},
    {0, 190},
    {164, 20},
    {261, 20},
    {391, 20},
    {0, 95},
    {261, 20},
    {391, 20},
    {659, 20},
    {0, 95},
    {329, 20},
    {493, 20},
    {783, 20},
    {0, 95},
    {349, 20},
    {523, 20},
    {880, 20},
    {0, 190},
    {293, 20},
    {440, 20},
    {698, 20},
    {0, 47},
    {329, 20},
    {493, 20},
    {783, 20},
    {0, 189},
    {261, 20},
    {440, 20},
    {659, 20},
    {0, 189},
    {220, 20},
    {329, 20},
    {523, 20},
    {0, 47},
    {246, 20},
    {349, 20},
    {587, 20},
    {0, 47},
    {195, 20},
    {293, 20},
    {493, 20},
    {0, 332},
    {195, 20},
    {329, 20},
    {523, 20},
    {0, 332},
    {164, 20},
    {261, 20},
    {391, 20},
    {0, 332},
    {130, 20},
    {195, 20},
    {329, 20},
    {0, 332},
    {174, 20},
    {261, 20},
    {440, 20},
    {0, 189},
    {195, 20},
    {293, 20},
    {493, 20},
    {0, 189},
    {184, 20},
    {277, 20},
    {466, 20},
    {0, 47},
    {174, 20},
    {261, 20},
    {440, 20},
    {0, 190},
    {164, 20},
    {261, 20},
    {391, 20},
    {0, 95},
    {261, 20},
    {391, 20},
    {659, 20},
    {0, 95},
    {329, 20},
    {493, 20},
    {783, 20},
    {0, 95},
    {349, 20},
    {523, 20},
    {880, 20},
    {0, 190},
    {293, 20},
    {440, 20},
    {698, 20},
    {0, 47},
    {329, 20},
    {493, 20},
    {783, 20},
    {0, 189},
    {261, 20},
    {440, 20},
    {659, 20},
    {0, 189},
    {220, 20},
    {329, 20},
    {523, 20},
    {0, 47},
    {246, 20},
    {349, 20},
    {587, 20},
    {0, 47},
    {195, 20},
    {293, 20},
    {493, 20},
    {0, 332},
    {130, 95},
    {0, 190},
    {659, 20},
    {783, 20},
    {659, 20},
    {783, 20},
    {0, 47},
    {195, 20},
    {622, 20},
    {739, 20},
    {0, 47},
    {587, 20},
    {698, 20},
    {587, 20},
    {698, 20},
    {0, 47},
    {493, 20},
    {622, 20},
    {493, 20},
    {622, 20},
    {0, 47},
    {261, 95},
    {0, 47},
    {523, 20},
    {659, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {174, 95},
    {0, 47},
    {329, 20},
    {415, 20},
    {329, 20},
    {415, 20},
    {0, 47},
    {349, 20},
    {440, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 20},
    {391, 20},
    {523, 20},
    {0, 47},
    {261, 95},
    {0, 47},
    {261, 20},
    {440, 20},
    {261, 20},
    {440, 20},
    {0, 47},
    {174, 20},
    {329, 20},
    {523, 20},
    {0, 47},
    {349, 20},
    {587, 20},
    {349, 20},
    {587, 20},
    {0, 47},
    {130, 95},
    {0, 190},
    {659, 20},
    {783, 20},
    {659, 20},
    {783, 20},
    {0, 47},
    {164, 20},
    {622, 20},
    {739, 20},
    {0, 47},
    {587, 20},
    {698, 20},
    {587, 20},
    {698, 20},
    {0, 47},
    {493, 20},
    {622, 20},
    {493, 20},
    {622, 20},
    {0, 47},
    {195, 95},
    {0, 47},
    {261, 20},
    {523, 20},
    {659, 20},
    {0, 189},
    {698, 20},
    {783, 20},
    {1046, 20},
    {0, 189},
    {698, 20},
    {783, 20},
    {1046, 20},
    {0, 47},
    {698, 20},
    {783, 20},
    {1046, 20},
    {0, 190},
    {195, 95},
    {0, 190},
    {130, 95},
    {0, 190},
    {659, 20},
    {783, 20},
    {659, 20},
    {783, 20},
    {0, 47},
    {195, 20},
    {622, 20},
    {739, 20},
    {0, 47},
    {587, 20},
    {698, 20},
    {587, 20},
    {698, 20},
    {0, 47},
    {493, 20},
    {622, 20},
    {493, 20},
    {622, 20},
    {0, 47},
    {261, 95},
    {0, 47},
    {523, 20},
    {659, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {174, 95},
    {0, 47},
    {329, 20},
    {415, 20},
    {329, 20},
    {415, 20},
    {0, 47},
    {349, 20},
    {440, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 20},
    {391, 20},
    {523, 20},
    {0, 47},
    {261, 95},
    {0, 47},
    {261, 20},
    {440, 20},
    {261, 20},
    {440, 20},
    {0, 47},
    {174, 20},
    {329, 20},
    {523, 20},
    {0, 47},
    {349, 20},
    {587, 20},
    {349, 20},
    {587, 20},
    {0, 47},
    {130, 95},
    {0, 190},
    {207, 20},
    {415, 20},
    {622, 20},
    {0, 332},
    {233, 20},
    {349, 20},
    {587, 20},
    {0, 332},
    {261, 20},
    {329, 20},
    {523, 20},
    {0, 332},
    {195, 94},
    {0, 47},
    {195, 95},
    {0, 190},
    {130, 95},
    {0, 190},
    {130, 95},
    {0, 190},
    {659, 20},
    {783, 20},
    {659, 20},
    {783, 20},
    {0, 47},
    {195, 20},
    {622, 20},
    {739, 20},
    {0, 47},
    {587, 20},
    {698, 20},
    {587, 20},
    {698, 20},
    {0, 47},
    {493, 20},
    {622, 20},
    {493, 20},
    {622, 20},
    {0, 47},
    {261, 95},
    {0, 47},
    {523, 20},
    {659, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {174, 95},
    {0, 47},
    {329, 20},
    {415, 20},
    {329, 20},
    {415, 20},
    {0, 47},
    {349, 20},
    {440, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 20},
    {391, 20},
    {523, 20},
    {0, 47},
    {261, 95},
    {0, 47},
    {261, 20},
    {440, 20},
    {261, 20},
    {440, 20},
    {0, 47},
    {174, 20},
    {329, 20},
    {523, 20},
    {0, 47},
    {349, 20},
    {587, 20},
    {349, 20},
    {587, 20},
    {0, 47},
    {130, 95},
    {0, 190},
    {659, 20},
    {783, 20},
    {659, 20},
    {783, 20},
    {0, 47},
    {164, 20},
    {622, 20},
    {739, 20},
    {0, 47},
    {587, 20},
    {698, 20},
    {587, 20},
    {698, 20},
    {0, 47},
    {493, 20},
    {622, 20},
    {493, 20},
    {622, 20},
    {0, 47},
    {195, 95},
    {0, 47},
    {261, 20},
    {523, 20},
    {659, 20},
    {0, 189},
    {698, 20},
    {783, 20},
    {1046, 20},
    {0, 189},
    {698, 20},
    {783, 20},
    {1046, 20},
    {0, 47},
    {698, 20},
    {783, 20},
    {1046, 20},
    {0, 190},
    {195, 95},
    {0, 190},
    {130, 95},
    {0, 190},
    {659, 20},
    {783, 20},
    {659, 20},
    {783, 20},
    {0, 47},
    {195, 20},
    {622, 20},
    {739, 20},
    {0, 47},
    {587, 20},
    {698, 20},
    {587, 20},
    {698, 20},
    {0, 47},
    {493, 20},
    {622, 20},
    {493, 20},
    {622, 20},
    {0, 47},
    {261, 95},
    {0, 47},
    {523, 20},
    {659, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {174, 95},
    {0, 47},
    {329, 20},
    {415, 20},
    {329, 20},
    {415, 20},
    {0, 47},
    {349, 20},
    {440, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 20},
    {391, 20},
    {523, 20},
    {0, 47},
    {261, 95},
    {0, 47},
    {261, 20},
    {440, 20},
    {261, 20},
    {440, 20},
    {0, 47},
    {174, 20},
    {329, 20},
    {523, 20},
    {0, 47},
    {349, 20},
    {587, 20},
    {349, 20},
    {587, 20},
    {0, 47},
    {130, 95},
    {0, 190},
    {207, 20},
    {415, 20},
    {622, 20},
    {0, 332},
    {233, 20},
    {349, 20},
    {587, 20},
    {0, 332},
    {261, 20},
    {329, 20},
    {523, 20},
    {0, 332},
    {195, 94},
    {0, 47},
    {195, 95},
    {0, 190},
    {130, 95},
    {0, 190},
    {103, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 189},
    {155, 20},
    {415, 20},
    {523, 20},
    {0, 190},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {207, 20},
    {466, 20},
    {587, 20},
    {0, 190},
    {195, 20},
    {391, 20},
    {659, 20},
    {0, 47},
    {329, 20},
    {523, 20},
    {329, 20},
    {523, 20},
    {0, 190},
    {130, 20},
    {329, 20},
    {440, 20},
    {0, 47},
    {261, 20},
    {391, 20},
    {261, 20},
    {391, 20},
    {0, 189},
    {97, 95},
    {0, 189},
    {103, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 189},
    {155, 20},
    {415, 20},
    {523, 20},
    {0, 190},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {207, 20},
    {466, 20},
    {587, 20},
    {0, 47},
    {391, 20},
    {659, 20},
    {391, 20},
    {659, 20},
    {0, 47},
    {195, 95},
    {0, 333},
    {130, 95},
    {0, 332},
    {97, 95},
    {0, 189},
    {103, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 189},
    {155, 20},
    {415, 20},
    {523, 20},
    {0, 190},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {207, 20},
    {466, 20},
    {587, 20},
    {0, 190},
    {195, 20},
    {391, 20},
    {659, 20},
    {0, 47},
    {329, 20},
    {523, 20},
    {329, 20},
    {523, 20},
    {0, 190},
    {130, 20},
    {329, 20},
    {440, 20},
    {0, 47},
    {261, 20},
    {391, 20},
    {261, 20},
    {391, 20},
    {0, 189},
    {97, 95},
    {0, 189},
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 47},
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 189},
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 190},
    {146, 20},
    {369, 20},
    {523, 20},
    {0, 47},
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 190},
    {391, 20},
    {493, 20},
    {783, 20},
    {0, 475},
    {195, 20},
    {391, 20},
    {195, 20},
    {391, 20},
    {0, 473},
    {195, 20},
    {329, 20},
    {523, 20},
    {0, 332},
    {164, 20},
    {261, 20},
    {391, 20},
    {0, 332},
    {130, 20},
    {195, 20},
    {329, 20},
    {0, 332},
    {174, 20},
    {261, 20},
    {440, 20},
    {0, 189},
    {195, 20},
    {293, 20},
    {493, 20},
    {0, 189},
    {184, 20},
    {277, 20},
    {466, 20},
    {0, 47},
    {174, 20},
    {261, 20},
    {440, 20},
    {0, 190},
    {164, 20},
    {261, 20},
    {391, 20},
    {0, 95},
    {261, 20},
    {391, 20},
    {659, 20},
    {0, 95},
    {329, 20},
    {493, 20},
    {783, 20},
    {0, 95},
    {349, 20},
    {523, 20},
    {880, 20},
    {0, 190},
    {293, 20},
    {440, 20},
    {698, 20},
    {0, 47},
    {329, 20},
    {493, 20},
    {783, 20},
    {0, 189},
    {261, 20},
    {440, 20},
    {659, 20},
    {0, 189},
    {220, 20},
    {329, 20},
    {523, 20},
    {0, 47},
    {246, 20},
    {349, 20},
    {587, 20},
    {0, 47},
    {195, 20},
    {293, 20},
    {493, 20},
    {0, 332},
    {195, 20},
    {329, 20},
    {523, 20},
    {0, 332},
    {164, 20},
    {261, 20},
    {391, 20},
    {0, 332},
    {130, 20},
    {195, 20},
    {329, 20},
    {0, 332},
    {174, 20},
    {261, 20},
    {440, 20},
    {0, 189},
    {195, 20},
    {293, 20},
    {493, 20},
    {0, 189},
    {184, 20},
    {277, 20},
    {466, 20},
    {0, 47},
    {174, 20},
    {261, 20},
    {440, 20},
    {0, 190},
    {164, 20},
    {261, 20},
    {391, 20},
    {0, 95},
    {261, 20},
    {391, 20},
    {659, 20},
    {0, 95},
    {329, 20},
    {493, 20},
    {783, 20},
    {0, 95},
    {349, 20},
    {523, 20},
    {880, 20},
    {0, 190},
    {293, 20},
    {440, 20},
    {698, 20},
    {0, 47},
    {329, 20},
    {493, 20},
    {783, 20},
    {0, 189},
    {261, 20},
    {440, 20},
    {659, 20},
    {0, 189},
    {220, 20},
    {329, 20},
    {523, 20},
    {0, 47},
    {246, 20},
    {349, 20},
    {587, 20},
    {0, 47},
    {195, 20},
    {293, 20},
    {493, 20},
    {0, 332},
    {130, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {440, 20},
    {523, 20},
    {440, 20},
    {523, 20},
    {0, 190},
    {184, 20},
    {329, 20},
    {391, 20},
    {0, 47},
    {195, 95},
    {0, 190},
    {261, 20},
    {329, 20},
    {415, 20},
    {0, 190},
    {174, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {174, 95},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {261, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 94},
    {0, 47},
    {174, 95},
    {0, 190},
    {146, 20},
    {391, 20},
    {493, 20},
    {0, 95},
    {698, 20},
    {880, 20},
    {698, 20},
    {880, 20},
    {0, 95},
    {698, 20},
    {880, 20},
    {174, 15},
    {698, 15},
    {880, 15},
    {174, 47},
    {0, 47},
    {195, 20},
    {698, 20},
    {880, 20},
    {0, 95},
    {659, 20},
    {783, 20},
    {659, 20},
    {783, 20},
    {246, 95},
    {587, 20},
    {698, 20},
    {587, 20},
    {698, 20},
    {0, 95},
    {195, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {440, 20},
    {523, 20},
    {440, 20},
    {523, 20},
    {0, 47},
    {195, 95},
    {0, 47},
    {349, 20},
    {440, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 20},
    {329, 20},
    {391, 20},
    {0, 47},
    {261, 94},
    {0, 47},
    {195, 95},
    {0, 190},
    {130, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {440, 20},
    {523, 20},
    {440, 20},
    {523, 20},
    {0, 190},
    {184, 20},
    {329, 20},
    {391, 20},
    {0, 47},
    {195, 95},
    {0, 190},
    {261, 20},
    {329, 20},
    {415, 20},
    {0, 190},
    {174, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {174, 95},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {261, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 94},
    {0, 47},
    {174, 95},
    {0, 190},
    {195, 20},
    {391, 20},
    {493, 20},
    {0, 47},
    {195, 20},
    {587, 20},
    {698, 20},
    {0, 190},
    {195, 20},
    {587, 20},
    {698, 20},
    {0, 47},
    {195, 20},
    {587, 20},
    {698, 20},
    {0, 95},
    {220, 20},
    {523, 20},
    {659, 20},
    {0, 95},
    {246, 20},
    {493, 20},
    {587, 20},
    {0, 95},
    {261, 20},
    {391, 20},
    {523, 20},
    {0, 47},
    {329, 95},
    {0, 47},
    {195, 95},
    {0, 47},
    {329, 95},
    {0, 47},
    {130, 20},
    {261, 20},
    {130, 20},
    {261, 20},
    {0, 475},
    {130, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {440, 20},
    {523, 20},
    {440, 20},
    {523, 20},
    {0, 190},
    {184, 20},
    {329, 20},
    {391, 20},
    {0, 47},
    {195, 95},
    {0, 190},
    {261, 20},
    {329, 20},
    {415, 20},
    {0, 190},
    {174, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {174, 95},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {261, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 94},
    {0, 47},
    {174, 95},
    {0, 190},
    {146, 20},
    {391, 20},
    {493, 20},
    {0, 95},
    {698, 20},
    {880, 20},
    {698, 20},
    {880, 20},
    {0, 95},
    {698, 20},
    {880, 20},
    {174, 15},
    {698, 15},
    {880, 15},
    {174, 47},
    {0, 47},
    {195, 20},
    {698, 20},
    {880, 20},
    {0, 95},
    {659, 20},
    {783, 20},
    {659, 20},
    {783, 20},
    {246, 95},
    {587, 20},
    {698, 20},
    {587, 20},
    {698, 20},
    {0, 95},
    {195, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {440, 20},
    {523, 20},
    {440, 20},
    {523, 20},
    {0, 47},
    {195, 95},
    {0, 47},
    {349, 20},
    {440, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 20},
    {329, 20},
    {391, 20},
    {0, 47},
    {261, 94},
    {0, 47},
    {195, 95},
    {0, 190},
    {130, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {440, 20},
    {523, 20},
    {440, 20},
    {523, 20},
    {0, 190},
    {184, 20},
    {329, 20},
    {391, 20},
    {0, 47},
    {195, 95},
    {0, 190},
    {261, 20},
    {329, 20},
    {415, 20},
    {0, 190},
    {174, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {174, 95},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {261, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 94},
    {0, 47},
    {174, 95},
    {0, 190},
    {195, 20},
    {391, 20},
    {493, 20},
    {0, 47},
    {195, 20},
    {587, 20},
    {698, 20},
    {0, 190},
    {195, 20},
    {587, 20},
    {698, 20},
    {0, 47},
    {195, 20},
    {587, 20},
    {698, 20},
    {0, 95},
    {220, 20},
    {523, 20},
    {659, 20},
    {0, 95},
    {246, 20},
    {493, 20},
    {587, 20},
    {0, 95},
    {261, 20},
    {391, 20},
    {523, 20},
    {0, 47},
    {329, 95},
    {0, 47},
    {195, 95},
    {0, 47},
    {329, 95},
    {0, 47},
    {130, 20},
    {261, 20},
    {130, 20},
    {261, 20},
    {0, 475},
    {103, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 189},
    {155, 20},
    {415, 20},
    {523, 20},
    {0, 190},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {207, 20},
    {466, 20},
    {587, 20},
    {0, 190},
    {195, 20},
    {391, 20},
    {659, 20},
    {0, 47},
    {329, 20},
    {523, 20},
    {329, 20},
    {523, 20},
    {0, 190},
    {130, 20},
    {329, 20},
    {440, 20},
    {0, 47},
    {261, 20},
    {391, 20},
    {261, 20},
    {391, 20},
    {0, 189},
    {97, 95},
    {0, 189},
    {103, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 189},
    {155, 20},
    {415, 20},
    {523, 20},
    {0, 190},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {207, 20},
    {466, 20},
    {587, 20},
    {0, 47},
    {391, 20},
    {659, 20},
    {391, 20},
    {659, 20},
    {0, 47},
    {195, 95},
    {0, 333},
    {130, 95},
    {0, 332},
    {97, 95},
    {0, 189},
    {103, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 189},
    {155, 20},
    {415, 20},
    {523, 20},
    {0, 190},
    {415, 20},
    {523, 20},
    {415, 20},
    {523, 20},
    {0, 47},
    {207, 20},
    {466, 20},
    {587, 20},
    {0, 190},
    {195, 20},
    {391, 20},
    {659, 20},
    {0, 47},
    {329, 20},
    {523, 20},
    {329, 20},
    {523, 20},
    {0, 190},
    {130, 20},
    {329, 20},
    {440, 20},
    {0, 47},
    {261, 20},
    {391, 20},
    {261, 20},
    {391, 20},
    {0, 189},
    {97, 95},
    {0, 189},
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 47},
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 189},
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 190},
    {146, 20},
    {369, 20},
    {523, 20},
    {0, 47},
    {146, 20},
    {369, 20},
    {659, 20},
    {0, 190},
    {391, 20},
    {493, 20},
    {783, 20},
    {0, 475},
    {195, 20},
    {391, 20},
    {195, 20},
    {391, 20},
    {0, 473},
    {130, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {440, 20},
    {523, 20},
    {440, 20},
    {523, 20},
    {0, 190},
    {184, 20},
    {329, 20},
    {391, 20},
    {0, 47},
    {195, 95},
    {0, 190},
    {261, 20},
    {329, 20},
    {415, 20},
    {0, 190},
    {174, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {174, 95},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {261, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 94},
    {0, 47},
    {174, 95},
    {0, 190},
    {146, 20},
    {391, 20},
    {493, 20},
    {0, 95},
    {698, 20},
    {880, 20},
    {698, 20},
    {880, 20},
    {0, 95},
    {698, 20},
    {880, 20},
    {174, 15},
    {698, 15},
    {880, 15},
    {174, 47},
    {0, 47},
    {195, 20},
    {698, 20},
    {880, 20},
    {0, 95},
    {659, 20},
    {783, 20},
    {659, 20},
    {783, 20},
    {246, 95},
    {587, 20},
    {698, 20},
    {587, 20},
    {698, 20},
    {0, 95},
    {195, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {440, 20},
    {523, 20},
    {440, 20},
    {523, 20},
    {0, 47},
    {195, 95},
    {0, 47},
    {349, 20},
    {440, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 20},
    {329, 20},
    {391, 20},
    {0, 47},
    {261, 94},
    {0, 47},
    {195, 95},
    {0, 190},
    {130, 20},
    {523, 20},
    {659, 20},
    {0, 47},
    {440, 20},
    {523, 20},
    {440, 20},
    {523, 20},
    {0, 190},
    {184, 20},
    {329, 20},
    {391, 20},
    {0, 47},
    {195, 95},
    {0, 190},
    {261, 20},
    {329, 20},
    {415, 20},
    {0, 190},
    {174, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {174, 95},
    {0, 47},
    {523, 20},
    {698, 20},
    {523, 20},
    {698, 20},
    {0, 47},
    {261, 20},
    {349, 20},
    {440, 20},
    {0, 47},
    {261, 94},
    {0, 47},
    {174, 95},
    {0, 190},
    {195, 20},
    {391, 20},
    {493, 20},
    {0, 47},
    {195, 20},
    {587, 20},
    {698, 20},
    {0, 190},
    {195, 20},
    {587, 20},
    {698, 20},
    {0, 47},
    {195, 20},
    {587, 20},
    {698, 20},
    {0, 95},
    {220, 20},
    {523, 20},
    {659, 20},
    {0, 95},
    {246, 20},
    {493, 20},
    {587, 20},
    {0, 95},
    {261, 20},
    {391, 20},
    {523, 20},
    {0, 47},
    {329, 95},
    {0, 47},
    {195, 95},
    {0, 47},
    {329, 95},
    {0, 47},
    {130, 20},
    {261, 20},
    {130, 20},
    {261, 20},
    {0, 950}};
