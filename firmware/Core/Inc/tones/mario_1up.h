#pragma once
#include "tone.h"

static const volatile struct tone mario_1up[] = {
    {330, 125},
    {392, 125},
    {659, 125},
    {523, 125},
    {587, 125},
    {784, 125}
};
