
#pragma once
#include <inttypes.h>

#define BAT_ADC hadc1

enum
{
    BAT_LOW = 0,
    BAT_OK = 1
};

void bat_measure();
uint8_t bat_status();
uint8_t bat_soc();
float bat_volt();