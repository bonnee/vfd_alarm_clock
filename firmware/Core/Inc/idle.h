#pragma once

#include "dsplay/dsplay.h"
#include "tim.h"
#include "vfd.h"
#include <inttypes.h>

#define IDLE_TIMEOUT 30000      // Time to wait before dimming the screen
#define IDLE_TIMEOUT_HOME 45000 // Time to wait before going to the main display

#define IDLE_DIMMING_HIGH VFD_DIMMING_80
#define IDLE_DIMMING_LOW VFD_DIMMING_20

/**
 * @brief Start measuring idle time
 *
 */
void idle_start_count();
