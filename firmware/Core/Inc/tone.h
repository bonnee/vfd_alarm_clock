
#pragma once
#include <fsm.h>
#include <inttypes.h>
#include <stdbool.h>

#define BUZZER_TIMER_FREQ(tim) (HAL_RCC_GetPCLK2Freq() / ((tim.Init.Prescaler + 1)))
#define BUZZER_PERIOD(tim, freq) ((BUZZER_TIMER_FREQ(tim) / freq) - 1)
#define BUZZER_DUTY(tim, freq) ((BUZZER_PERIOD(tim, freq) * 50) / 100)

#define SET_FREQUENCY(htim, freq)                       \
    {                                                   \
        htim.Instance->ARR = BUZZER_PERIOD(htim, freq); \
        htim.Instance->CCR1 = BUZZER_DUTY(htim, freq);  \
    }

void alarm_entry(fsm fsm);
struct tone
{
    uint16_t note;
    uint16_t duration;
};

extern volatile struct tone *tone_queue;

void tone_run(uint16_t tone_length, bool repeat);
bool tone_is_running();
void tone_stop();
void tone_tick();
