/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file    tim.h
 * @brief   This file contains all the function prototypes for
 *          the tim.c file
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TIM_H__
#define __TIM_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern TIM_HandleTypeDef htim1;

extern TIM_HandleTypeDef htim2;

extern TIM_HandleTypeDef htim10;

extern TIM_HandleTypeDef htim11;

/* USER CODE BEGIN Private defines */

#define BUZZER_PWM_TIM htim10
#define BUZZER_PWM_CHANNEL HAL_TIM_ACTIVE_CHANNEL_1
#define BUZZER_PWM_CHANNEL_MASK TIM_CHANNEL_1

#define TONE_TIM htim1
#define TONE_CHANNEL HAL_TIM_ACTIVE_CHANNEL_1
#define TONE_CHANNEL_MASK TIM_CHANNEL_1

#define DISPLAY_TIM htim1
#define DISPLAY_CHANNEL HAL_TIM_ACTIVE_CHANNEL_2
#define DISPLAY_CHANNEL_MASK TIM_CHANNEL_2

#define IDLE_TIM htim2
#define IDLE_TIMEOUT_CHANNEL HAL_TIM_ACTIVE_CHANNEL_1
#define IDLE_TIMEOUT_CHANNEL_MASK TIM_CHANNEL_1
#define IDLE_HOME_CHANNEL HAL_TIM_ACTIVE_CHANNEL_2
#define IDLE_HOME_CHANNEL_MASK TIM_CHANNEL_2

#define ALARM_TIMEOUT_TIM htim2
#define ALARM_TIMEOUT_CHANNEL HAL_TIM_ACTIVE_CHANNEL_3
#define ALARM_TIMEOUT_CHANNEL_MASK TIM_CHANNEL_3

#define SIREN_TIM htim11
#define SIREN_CHANNEL HAL_TIM_ACTIVE_CHANNEL_1
#define SIREN_CHANNEL_MASK TIM_CHANNEL_1
/* USER CODE END Private defines */

void MX_TIM1_Init(void);
void MX_TIM2_Init(void);
void MX_TIM10_Init(void);
void MX_TIM11_Init(void);

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* USER CODE BEGIN Prototypes */

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __TIM_H__ */

